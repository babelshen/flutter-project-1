class User {
  String firstName;
  String lastName;
  int age;
  bool _study;

  User({required this.firstName, required this.lastName, required this.age, required bool study}) 
    : _study = study;

  bool get getStudyStatus => _study;

  set setStudyStatus(bool studyStatus) {
    _study = studyStatus;
  }
}
