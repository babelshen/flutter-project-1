import 'package:flutter/material.dart';
import 'user.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  User user = User(
  firstName: 'Illia',
  lastName: 'Babelnyk',
  age: 27,
  study: true,
);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor: const Color.fromRGBO(255, 255, 255, .9),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Empat School | Project 1', style: TextStyle(fontWeight: FontWeight.bold)),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 96, 67, 202),
        ),
        body: Center(
          child: Text(
            'First name: ${user.firstName}\nLast name: ${user.lastName}\nAge: ${user.age}\nStatus: ${user.getStudyStatus ? 'Study Flutter' : 'Retreat'}',
            style: const TextStyle(
              fontSize: 20,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: const Color.fromARGB(255, 80, 39, 230),
          onPressed: () {
            setState(() {
              bool studyStatus = user.getStudyStatus;
              user.setStudyStatus = !studyStatus;
            });
          },
          child: const Icon(Icons.assignment_ind_rounded, size: 40, color: Colors.white)
        ),
      )
    );
  }
}
